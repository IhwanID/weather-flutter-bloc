import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutterweather/blocs/blocs.dart';
import 'package:flutterweather/repositories/repositories.dart';
import 'package:flutterweather/simple_bloc_delegate.dart';
import 'package:flutterweather/ui/weather_screen.dart';
import 'package:http/http.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final WeatherRepository weatherRepository = WeatherRepository(
    weatherApiClient: WeatherApiClient(
      httpClient: Client(),
    ),
  );
  runApp(MultiBlocProvider(
    child: MyApp(weatherRepository: weatherRepository),
    providers: [
      BlocProvider<SettingBloc>(
        create: (context) => SettingBloc(),
      ),
      BlocProvider<ThemeBloc>(
        create: (context) => ThemeBloc(),
      )
    ],
  ));
}

class MyApp extends StatelessWidget {
  final WeatherRepository weatherRepository;

  MyApp({Key key, @required this.weatherRepository})
      : assert(weatherRepository != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(builder: (context, themeState) {
      return MaterialApp(
        title: 'Flutter Weather',
        theme: themeState.theme,
        home: BlocProvider(
          create: (context) =>
              WeatherBloc(weatherRepository: weatherRepository),
          child: WeatherScreen(),
        ),
      );
    });
  }
}
